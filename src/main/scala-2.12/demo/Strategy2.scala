package demo

import structural.Application

/**
  * Created by macblady on 23.04.2017.
  */
object Strategy2 {
  def main(args: Array[String]): Unit = {
    Application.run()
  }
}
