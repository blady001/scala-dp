package demo

import structural._

/**
  * Created by macblady on 03.04.2017.
  */
object Decorator {
  def main(args: Array[String]): Unit ={
    // input reader test
    val ireader = new InputReader()
    println("Input reader")
    println(ireader.read())

    val capitalizedIreader = new CapitalizeInputReader(ireader)
    println("Capitalize input reader")
    println(capitalizedIreader.read())

    val whitespaceCleanInputReader = new WhitespaceCleanInputReader(ireader)
    println("whitespace reader")
    println(whitespaceCleanInputReader.read())

    val fileReader = new FileReader("file1.txt")
    println("filereader")
    println(fileReader.read())

    val capitalizedFileReader = new CapitalizeFileReader(fileReader)
    println("capitalized file reader")
    println(capitalizedFileReader.read())

    val whitespaceCleanFileReader = new WhitespaceCleanFileReader(fileReader)
    println("whitespace file reader")
    println(whitespaceCleanFileReader.read())

  }
}
