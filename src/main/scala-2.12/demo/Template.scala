package demo

import behavioral.{BaseCls, SubCls}

/**
  * Created by macblady on 10.04.2017.
  */
object Template {
  def main(args: Array[String]): Unit = {
    val baseCls = new BaseCls()
    val subCls = new SubCls()
    baseCls.run()
    subCls.run()
  }
}
