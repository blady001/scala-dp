package demo

import structural.{GermanBaseDude, IrishDude, PolishDude}

/**
  * Created by macblady on 10.04.2017.
  */
object Strategy {
  def main(args: Array[String]): Unit = {
    val irishDude = new IrishDude()
    val polishDude = new PolishDude()
    val germanDude = new GermanBaseDude()
    irishDude.speak()
    polishDude.speak()
    germanDude.speak()
  }
}
