package demo

import creational.{CloneProduct, ProductPrototype}

/**
  * Created by macblady on 23.04.2017.
  */
object Prototype {

  implicit def cloneProduct(species: ProductPrototype): CloneProduct = new CloneProduct(species)

  def main(args: Array[String]){

    var prod1 = new ProductPrototype("big joe")
    prod1.method
    var prod1clone = prod1.copy
    prod1clone.method

  }
}