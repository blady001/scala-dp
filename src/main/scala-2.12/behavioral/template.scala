package behavioral

/**
  * Created by macblady on 10.04.2017.
  */

class BaseCls {
  def setUp(): Unit = {
    println("Doing setup!")
  }
  def cleanUp(): Unit = {
    println("Doing cleanup!")
  }
  def mainStuff(): Unit = {
    println("BaseCls, doing nothing!")
  }
  def run(): Unit = {
    setUp()
    mainStuff()
    cleanUp()
  }
}

class SubCls extends BaseCls {
  override def mainStuff(): Unit = {
    println("Doing main stuff from the subclass!")
  }
}
