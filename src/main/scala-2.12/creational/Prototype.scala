package creational

/**
  * Created by macblady on 23.04.2017.
  */


class ProductPrototype(var name: String){
  def method = {
    println("my name is " + name)
  }
}

class CloneProduct(species: ProductPrototype){
  def copy = new ProductPrototype(species.name)
}