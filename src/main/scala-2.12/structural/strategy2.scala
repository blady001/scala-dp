package structural

/**
  * Created by macblady on 23.04.2017.
  */

class MParser {

  def parse(filename: String): String = {
    ""
  }

  def parseFile(filename: String): String = {

    if (filename.endsWith(".json")) {
      val parser = new JsonParser()
      return parser.parse(filename)
    }
    else {
      val parser = new CSVParser(",")
      return parser.parse(filename)
    }
  }

}

class CSVParser(delimeter: String) extends MParser {
  override def parse(filename: String): String = {
//    val lines = scala.io.Source.fromFile("file.txt").getLines()
    println("Parsing CSV...")
    ""
  }
}

class JsonParser extends MParser {
  override def parse(filename: String): String = {
    println("Parsing JSON...")
    ""
  }
}

object Application {
  def run(): Unit = {
    val p = new MParser()
    p.parseFile("sth.json")
    p.parseFile("sth.csv")
  }
}