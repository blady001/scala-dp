package structural

import java.io.{BufferedReader, InputStreamReader}

/**
  * Created by macblady on 03.04.2017.
  */

trait Reader { def read(): String }

class FileReader(filename: String) extends Reader {

  override def read(): String = {
//    scala.io.Source.fromFile(filename).mkString
    val stream = this.getClass.getResourceAsStream(filename)
    val reader = new BufferedReader(
      new InputStreamReader(
        stream
      )
    )
    try {
      val s = reader.readLine()
      return s
    } finally {
      reader.close()
      stream.close()
    }
  }
}

class CapitalizeFileReader(reader: Reader) extends Reader {
  override def read(): String = {
    val content = reader.read()
    content.split(' ').map(_.capitalize).mkString(" ")
  }
}

class WhitespaceCleanFileReader(reader: Reader) extends Reader {
  override def read(): String = {
    val content = reader.read()
    content.replaceAll("\\s", "")
  }
}

class InputReader extends Reader {
  override def read(): String = {
    val text = scala.io.StdIn.readLine()
    return text
  }
}

class CapitalizeInputReader(reader: Reader) extends Reader {
  override def read(): String = {
    val content = reader.read()
    content.split(' ').map(_.capitalize).mkString(" ")
  }
}

class WhitespaceCleanInputReader(reader: Reader) extends Reader {
  override def read(): String = {
    val content = reader.read()
    content.replaceAll("\\s", "")
  }
}
