package structural

/**
  * Created by macblady on 10.04.2017.
  */
class strategy {

}

trait SpeakBehavior { def speak(): String }

class NormalSpeaking extends SpeakBehavior {
  override def speak(): String = {
    "Hello"
  }
}

class IrishSpeaking extends SpeakBehavior {
  override def speak(): String = {
    "Wer is my beer!"
  }
}

class PolishSpeaking extends SpeakBehavior {
  override def speak(): String = {
    "Jakiś problem?!"
  }
}

class GermanSpeaking extends SpeakBehavior {
  override def speak(): String = {
    "SCHMETTERLIGN!"
  }
}

trait BaseDude {
  val speakBehavior: SpeakBehavior

  def speak(): Unit = {
    println(speakBehavior.speak())
  }
}

class GermanBaseDude extends BaseDude {
  val speakBehavior = new GermanSpeaking
}

class IrishDude extends BaseDude {
  val speakBehavior = new IrishSpeaking
}

class PolishDude extends BaseDude {
  val speakBehavior = new PolishSpeaking
}

